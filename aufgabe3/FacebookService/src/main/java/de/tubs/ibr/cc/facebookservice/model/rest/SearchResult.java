package de.tubs.ibr.cc.facebookservice.model.rest;

import de.tubs.ibr.cc.facebookservice.model.Person;

import java.util.List;

public class SearchResult {
	public final List<Person> result;
	public final int offset;
	public final int length;

	public SearchResult(List<Person> result, int offset, int length) {
		this.result = result;
		this.offset = offset;
		this.length = length;
	}
}
