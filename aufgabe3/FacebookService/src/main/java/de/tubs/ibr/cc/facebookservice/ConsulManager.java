package de.tubs.ibr.cc.facebookservice;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.catalog.model.CatalogRegistration;
import de.tubs.ibr.cc.facebookservice.configuration.properties.ConsulConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class ConsulManager {

	private static final Logger log = LoggerFactory.getLogger(ConsulManager.class);
	public static final String NODE_NAME = "facebook-node";

	private final ConsulConfig consulConfig;

	@Autowired
	public ConsulManager(ConsulConfig consulConfig) {
		this.consulConfig = consulConfig;
	}


	@PostConstruct
	public void RegisterWithConsul() {
		ConsulClient consulClient = new ConsulClient(consulConfig.getAgentAddress(), consulConfig.getAgentPort());
		CatalogRegistration catalogRegistration = new CatalogRegistration();
		if (StringUtils.isEmpty(catalogRegistration.getAddress())) {
			log.warn("No service-address set in configuration; this will likely cause registration failure");
		}
		catalogRegistration.setAddress(consulConfig.getServiceAddress());
		catalogRegistration.setNode(NODE_NAME);

		CatalogRegistration.Service service = new CatalogRegistration.Service();
		service.setService(consulConfig.getServiceName());
		service.setId(consulConfig.getServiceId());
		service.setPort(consulConfig.getServicePort());

		catalogRegistration.setService(service);

		if (StringUtils.isEmpty(consulConfig.getToken())) {
			log.warn("No consul token found. Trying to register with consul without token.");
			consulClient.catalogRegister(catalogRegistration);
		} else {
			consulClient.catalogRegister(catalogRegistration, consulConfig.getToken());
		}
		log.info("Registered with Consul: " + catalogRegistration);
	}

	@PreDestroy
	public void unregisterFromConsul() {
		Deregister deregister = new Deregister(NODE_NAME);
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Consul-Token", consulConfig.getToken());
		HttpEntity<Deregister> entity = new HttpEntity<>(deregister, headers);
		String url = "http://" + consulConfig.getAgentAddress() + ":" + consulConfig.getAgentPort() + "/v1/catalog/deregister";
		ResponseEntity<String> respEntity = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
		String resp = respEntity.getBody();
		log.info("Deregistered from Consul: " + resp);
	}

	public static class Deregister {
		public final String Node;

		public Deregister(String node) {
			Node = node;
		}
	}
}
