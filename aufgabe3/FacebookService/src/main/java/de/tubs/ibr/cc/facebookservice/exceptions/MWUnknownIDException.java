package de.tubs.ibr.cc.facebookservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Unknown ID")
public class MWUnknownIDException extends RuntimeException {

	public MWUnknownIDException(String message) {
		super(message);
	}

}
