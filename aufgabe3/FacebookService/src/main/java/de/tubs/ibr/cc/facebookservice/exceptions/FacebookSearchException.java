package de.tubs.ibr.cc.facebookservice.exceptions;

public class FacebookSearchException extends RuntimeException {
	public FacebookSearchException(Throwable e) {
		super(e);
	}
}
