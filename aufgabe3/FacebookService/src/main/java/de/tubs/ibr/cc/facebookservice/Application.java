package de.tubs.ibr.cc.facebookservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/*
 * Enables swagger through springfox
 * Default start path: http://localhost:8080/v2/api-docs
 * If swagger-ui is in classpath, it's reachable by default on http://localhost:8080/swagger-ui.html
 */
@EnableSwagger2
/*
 * SpringBootApplication is equivalent to using:
 * - EnableAutoConfiguration: (Auto-)Configures modules found in classpath by conventional standards
 * - ComponentScan: Scans for components - default scan path is the path this file resides in
 * - Configuration: Bean registration, defining configuration possible in this class
 */
@SpringBootApplication // EnableAutoConfiguration, ComponentScan, Configuration
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class);
  }


  // specifically used to tell springfox to create swagger endpoints
  @Bean
  public Docket swaggerApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any()) // use any kind of request (POST, GET,...)
        .paths(PathSelectors.ant("/facebook/**")) // use all available paths
        .build();
  }

}
