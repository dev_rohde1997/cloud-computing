package de.tubs.ibr.cc.facebookservice.model.rest;

import java.util.Set;

public class Friends {
	public final Set<String> ids;

	public Friends(Set<String> ids) {
		this.ids = ids;
	}
}
