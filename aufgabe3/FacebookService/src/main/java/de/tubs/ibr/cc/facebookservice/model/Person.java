package de.tubs.ibr.cc.facebookservice.model;

import java.util.HashSet;
import java.util.Set;

public class Person {
	private String id;
	private String name;
	private Set<String> friends;

	public Person() {}

	public Person(String id, String name) {
		this.id = id;
		this.name = name;
		friends = new HashSet<>();
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<String> getFriends() {
		return friends;
	}
}
