package de.tubs.ibr.cc.facebookservice.repositories;

import de.tubs.ibr.cc.facebookservice.configuration.properties.ApplicationProperties;
import de.tubs.ibr.cc.facebookservice.exceptions.FacebookException;
import de.tubs.ibr.cc.facebookservice.model.Person;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class FacebookRepository {
	private final ApplicationProperties applicationProperties;
	private Map<String, Person> people;

	private static final Logger log = LoggerFactory.getLogger(FacebookRepository.class);

	public FacebookRepository(ApplicationProperties applicationProperties) {
		this.applicationProperties = applicationProperties;

		System.out.println(applicationProperties.getDataDirectory());

		people = readPeople();
	}

	public Person getPerson(String id) {
		return people.get(id);
	}

	/**
	 * Guarantees preserved iteration order over multiple calls
	 */
	public Collection<Person> getAll() {
		return people.values();
	}


	private Map<String, Person> readPeople() {
		File namesFile = new File(applicationProperties.getDataDirectory(), "names.list");
		File friendsFile = new File(applicationProperties.getDataDirectory(), "friends.list");
		File friendCountFile = new File(applicationProperties.getDataDirectory(), "friendcounts.list");

		Map<String, Person> peopleMap = new LinkedHashMap<>();

		log.debug("Reading Names from '" + namesFile.getAbsolutePath() + "'...");

		try (LineIterator it = FileUtils.lineIterator(namesFile, "UTF-8");) {
			while (it.hasNext()) {
				String nameLine = it.nextLine();
				String[] data = nameLine.split("\t");
				Person person = new Person(data[0], data[1]);
				peopleMap.put(person.getId(), person);
			}
		} catch (IOException e) {
			log.error("Could not read names file at " + namesFile.getAbsolutePath(), e);
			throw new FacebookException();
		}
		log.debug("Read " + peopleMap.size() + " people.");
		log.debug("Reading Friendlist...");

		int cnt = 0;
		try (LineIterator it = FileUtils.lineIterator(friendsFile, "UTF-8");) {
			Person lastPerson = null;
			while (it.hasNext()) {
				String friendLine = it.nextLine();
				String[] data = friendLine.split("\t");
				String personId = data[0];
				String friendId = data[1];
				peopleMap.get(friendId).getFriends().add(personId);
				if (lastPerson == null || !personId.equals(lastPerson.getId())) {
					lastPerson = peopleMap.get(personId);
				}
				lastPerson.getFriends().add(friendId);
				cnt++;
			}
		} catch (IOException e) {
			log.error("Could not read friend list file at " + friendsFile.getAbsolutePath(), e);
			throw new FacebookException();
		}

		log.debug("Read " + cnt + " lines of Friends.");

		return peopleMap;
	}
}
