package de.tubs.ibr.cc.facebookservice.service.friends;

import de.tubs.ibr.cc.facebookservice.exceptions.FacebookSearchException;
import de.tubs.ibr.cc.facebookservice.exceptions.PersonNotFoundException;
import de.tubs.ibr.cc.facebookservice.model.Person;
import de.tubs.ibr.cc.facebookservice.repositories.FacebookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;


/**
 * Facebook friends service.
 * <p>
 * Friends should be initialized by the time the service is used.
 */
@Service
public class FriendsService {

  private final FacebookRepository repo;
  private static final Logger log = LoggerFactory.getLogger(FriendsService.class);

  public FriendsService(FacebookRepository repo) {
    this.repo = repo;
  }

  public Set<String> getFriendIds(Collection<String> ids) {
    Set<String> friendIds = ids.stream()
        .map(id -> getPerson(id).getFriends())
        .flatMap(friends -> friends.stream())
        .collect(Collectors.toSet());

    return friendIds;
  }

  public List<Person> search(String regex, int offset, int length) {
    try {
      log.debug("Searching People using '" + regex + "'.");
      Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
      Collection<Person> all = repo.getAll();
      return all.stream()
          .filter(p -> pattern.matcher(p.getName()).matches())
          .skip(offset)
          .limit(length)
          .collect(Collectors.toList());
    } catch (PatternSyntaxException e) {
      log.error("Provided regex invalid '" + regex + "'.", e);
      throw new FacebookSearchException(e);
    }
  }


  public Person getPerson(String id) {
    Person person = repo.getPerson(id);
    if (person == null) {
      throw new PersonNotFoundException(id);
    }
    return person;
  }
}
