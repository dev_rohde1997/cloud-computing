package de.tubs.ibr.cc.facebookservice.controller;

import de.tubs.ibr.cc.facebookservice.model.Person;
import de.tubs.ibr.cc.facebookservice.model.rest.Friends;
import de.tubs.ibr.cc.facebookservice.model.rest.SearchResult;
import de.tubs.ibr.cc.facebookservice.service.friends.FriendsService;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@RequestMapping("facebook")
public class FacebookController {

  private final FriendsService friendsService;


  public FacebookController(final FriendsService friendsService) {
    this.friendsService = friendsService;
  }


  @GetMapping("/user/{id}")
  public Person get(@PathVariable final String id) {
    return friendsService.getPerson(id);
  }


  // TODO body parameter instead of query?
  @PostMapping("/users")
  public List<Person> userBatch(@RequestBody final List<String> ids) {
    return ids.stream().map(friendsService::getPerson).collect(Collectors.toList());
  }


  // TODO caching?
  @GetMapping("/search")
  public SearchResult search(@RequestParam(name = "regex", required = false) final String regex,
      @RequestParam(name = "query", required = false) final String query,
      @RequestParam(name = "offset", defaultValue = "0") final int offset,
      @RequestParam(name = "length", defaultValue = "10") final int length) {
    List<Person> people = null;

    if (regex != null) {
      people = friendsService.search(regex, offset, length);
    } else if (query != null) {
      people = friendsService.search(query + ".*", offset, length);
    } else {
      people = Collections.emptyList();
    }
    return new SearchResult(people, offset, length);
  }


  @PostMapping("/friends")
  // Post+RequestBody since RequestParam/HttpClient have limits of 8000-9000 characters
  public Friends getFriendsBatch(@RequestBody final List<String> ids) {
    System.out.println(ids.size());
    Set<String> friendsBatch = friendsService.getFriendIds(ids);
    return new Friends(friendsBatch);
  }


}
