package de.tubs.ibr.cc.facebookservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class PersonNotFoundException extends RuntimeException {
	public PersonNotFoundException(String id) {
		super("Person " + id + " does not exist.");
	}
}
