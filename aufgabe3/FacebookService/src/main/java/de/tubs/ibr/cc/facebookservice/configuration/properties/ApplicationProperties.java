package de.tubs.ibr.cc.facebookservice.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
  @NonNull private String dataDirectory;


  public String getDataDirectory() {
    return dataDirectory;
  }


  public void setDataDirectory(String dataDirectory) {
    this.dataDirectory = dataDirectory;
  }
}
