server = true
bootstrap_expect =  {{ groups['consul_server'] | length }}

acl {
    tokens {
        master = "{{ consul_operator_token }}"
    }
}

enable_script_checks = true
