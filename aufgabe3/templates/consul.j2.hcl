datacenter = "ibr"
data_dir = "/opt/consul/"

node_name = "{{ node_name }}"

acl {
    tokens {
        master = "{{ consul_operator_token }}"
    }
}

enable_script_checks = true

