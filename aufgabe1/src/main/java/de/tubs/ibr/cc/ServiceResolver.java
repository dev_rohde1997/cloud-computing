package main.java.de.tubs.ibr.cc;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.constraints.Null;

public class ServiceResolver {

    //CONSTANTS

    //URI - Values
    public static final String BASE_URI = "http://134.169.35.200:8500";
    public final static String SUB_URI_SERVICE = "/v1/catalog/service/";
    public final static String SUB_URI_SERVICES = "/v1/catalog/services";
    public final static String SUB_URI_API = "/v2/api-docs";

    //TOKEN - Values
    public final static String TOKEN_VALUE = "2905b0d2-89c7-6b07-4a7a-e4684cc5e2d9";
    public final static String HEADER_X_CONSUL_TOKEN_NAME = "X-Consul-Token";
    public final static String HEADER_CONTENT_TYPE = "Content-Type";
    public final static String HEADER_CONTENT_TYPE_JSON = "application/json";

    //Name of our Service
    public static final String SERVICE_NAME = "FacebookService";


    //Vars
    private HttpClient client = HttpClient.newHttpClient();

    //METHODS

    //List of all services addresses
    private String getServiceAddress(String serviceName) throws JsonProcessingException{
        FacebookService service = getService(serviceName);
        return service.getIP();
    }

    //List of all services endpoints
    public ServiceApiDocumentation listServiceEndpoints(String serviceName) throws JsonProcessingException{
        String api_response = getApiDocs(getServiceAddress(serviceName));

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(api_response, ServiceApiDocumentation.class);


    }

    //List of all available services
    public Set<String> listServices() throws JsonProcessingException {

        String serviceAddress = startHttpRequest_GET(BASE_URI  + SUB_URI_SERVICES);
        if (serviceAddress == null) return null;

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> jsonMap = mapper.readValue(serviceAddress,
                new TypeReference<>(){});

        return jsonMap.keySet();

    }

    //Search for person by name
    SearchResult searchPerson(String name, int length, int offset, String regex)
            throws JsonProcessingException, IllegalArgumentException {

        String uri = buildURI(name, length, offset, regex);
        String serviceString = startHttpRequest_GET(uri);
        assert serviceString != null;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, SearchResult.class);
    }

    //Search for friends by user id
    Person searchFriends(String user_id)
            throws JsonProcessingException, IllegalArgumentException {

        String uri = getServiceAddress(SERVICE_NAME);
        uri += "/facebook/user/" + user_id;

        String serviceString = startHttpRequest_GET(uri);
        assert serviceString != null;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, Person.class);
    }

    //Search for friends batches by user ids
    Friends searchFriendsBatch(List<String> user_ids)
            throws JsonProcessingException, IllegalArgumentException {

        String uri = getServiceAddress(SERVICE_NAME);
        uri += "/facebook/friends";
        StringBuilder json = new StringBuilder("");
        addArrayToJSON(user_ids, json);

        String serviceString = startHttpRequest_POST(uri, json.toString());
        assert serviceString != null;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, Friends.class);
    }

    public String getApiDocs(final String URL) {
            return startHttpRequest_GET(URL + SUB_URI_API);
    }


    //HELPER METHODS

    //Simple GET Request
    private String startHttpRequest_GET(String uri) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .header(HEADER_X_CONSUL_TOKEN_NAME, TOKEN_VALUE)
                    .build();
            return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (InterruptedException|IOException| NullPointerException e1 ) {
            e1.printStackTrace();
        }
        return null;
    }

    //Simple POST Request
    private String startHttpRequest_POST(String uri, String json) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .header(HEADER_X_CONSUL_TOKEN_NAME, TOKEN_VALUE)
                    .header(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_JSON)
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .build();
            return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (InterruptedException|IOException|NullPointerException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    //Requesting facebook service data
    private FacebookService getService(String service_name) throws JsonProcessingException {
        if (service_name == null) return null;
        String uri = BASE_URI + SUB_URI_SERVICE + service_name;
        String serviceString = startHttpRequest_GET(uri);
        if (serviceString == null) return null;

        serviceString = serviceString.substring(1,serviceString.length() - 1);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, FacebookService.class);
    }

    //Building a URI by given params
    private String buildURI(String name, int length, int offset, String regex) throws JsonProcessingException {
        StringBuilder uri = new StringBuilder(getServiceAddress(SERVICE_NAME));
        uri.append("/facebook/search");

        if (name == null) throw new IllegalArgumentException();
        uri.append("?");
        if (length > 0) {
            uri.append("length=");
            uri.append(length);
            uri.append("&");
        }

        if (offset > 0) {
            uri.append("offset=0");
            uri.append("&");
        }

        uri.append("query=");
        uri.append(name);
        uri.append("&");


        if (regex != null) {
            uri.append("regex=");
            uri.append(regex);
            uri.append("&");
        }

        uri.delete(uri.length()-1, uri.length());
        System.out.println(uri.toString());
        return uri.toString();
    }

    //Printing a json values of a facebook service
    private void printService(FacebookService facebookService) {
        System.out.println("ID: " + facebookService.getID());
        System.out.println("Node: " + facebookService.getNode());
        System.out.println("Address: " + facebookService.getAddress());
        System.out.println("Datacenter: " + facebookService.getDatacenter());
        System.out.println("TaggedAddresses: " + facebookService.getTaggedAddresses());
        System.out.println("NodeMeta: " + facebookService.getNodeMeta());
        System.out.println("ServiceKind: " + facebookService.getServiceKind());
        System.out.println("ServiceID: " + facebookService.getServiceID());
        System.out.println("ServiceName: " + facebookService.getServiceName());
        System.out.println("ServiceTags: " + facebookService.getServiceTags());
        System.out.println("ServiceAddress: " + facebookService.getServiceAddress());
        System.out.println("ServiceWeights: " + facebookService.getServiceWeights());
        System.out.println("ServiceMeta: " + facebookService.getServiceMeta());
        System.out.println("ServicePort: " + facebookService.getServicePort());
        System.out.println("ServiceEnableTagOverride: " + facebookService.getServiceEnableTagOverride());
        System.out.println("ServiceProxyDestination: " + facebookService.getServiceProxyDestination());
        System.out.println("ServiceProxy: " + facebookService.getServiceProxy());
        System.out.println("ServiceConnect: " + facebookService.getServiceConnect());
        System.out.println("CreateIndex: " + facebookService.getCreateIndex());
        System.out.println("ModifyIndex: " + facebookService.getModifyIndex());
    }

    //Building a json string by a given List of Strings
    private void addArrayToJSON(List<String> user_ids, StringBuilder json) {
        json.append("[");
        for (String s : user_ids) {
            json.append("\"");
            json.append(s);
            json.append("\"");
            json.append(",");
        }
        json.delete(json.length()-1, json.length()); //Remove last ,
        json.append("]");
    }

}
