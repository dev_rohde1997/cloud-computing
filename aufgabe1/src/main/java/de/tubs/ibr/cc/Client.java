package main.java.de.tubs.ibr.cc;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.*;

/**
 * CLI application entry point
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("Facebook Command Line Client");

        //Init args
        if (args.length < 1 | args.length > 3) throw new IllegalArgumentException();

        String option_1 = args[0];
        String option_2 = null;
        if (args.length > 1) {
            option_2 = args[1];
        }

        try {
            ServiceResolver serviceResolver = new ServiceResolver();
            switch (option_1) {
                case "endpoints":
                    System.out.println("List of all endpoints:");
                    List<String> endpoints = getEndpoints(option_2);
                    printListFormat(endpoints);
                    break;
                case "services":
                    Set<String> service_list = serviceResolver.listServices();
                    printListFormat(service_list);
                    break;
                case "search":
                    System.out.println("Searching for " + option_2);
                    System.out.println(searchPersons(option_2));
                    break;
                case "friends":
                    System.out.println("Searching for friends of " + option_2);
                    List<String> friends = getFriends(option_2);
                    printListFormat(friends);
                    break;
                case "friends_batch":
                    System.out.println("Searching for friends batch");
                    Person p  = searchPersons("Moritz").get(0);
                    printListFormat(getFriendsBatch(p.getFriendsList()));
                    break;
                default:
                    System.err.println("Unknown option_1");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<Person> searchPersons(String name) throws JsonProcessingException {
        ServiceResolver serviceResolver = new ServiceResolver();
        SearchResult result = serviceResolver.searchPerson(name, 10, 0, null);
        return result.getResult();
    }

    private static List<String> getFriends(String id) throws JsonProcessingException {
        ServiceResolver serviceResolver = new ServiceResolver();
        Person p = serviceResolver.searchFriends(id);
        return p.getFriends();
    }

    private static List<String> getFriendsBatch(List<String> ids) throws JsonProcessingException {
        ServiceResolver serviceResolver = new ServiceResolver();
        Friends friends = serviceResolver.searchFriendsBatch(ids);
        return friends.getIds();
    }

    private static List<String> getEndpoints(String service_name) throws JsonProcessingException{
        ServiceResolver serviceResolver = new ServiceResolver();
        ServiceApiDocumentation docs = serviceResolver.listServiceEndpoints(service_name);
        Iterator<String> iterator = docs.getPaths().fieldNames();
        ArrayList<String> endpointList = new ArrayList<>();

        while (iterator.hasNext()) {
            endpointList.add(iterator.next());
        }

        return endpointList;
    }

    private static void printListFormat(Iterable<String> list) {
        for (String key : list) {
            System.out.println("\t-" + key);
        }
    }

}
