package main.java.de.tubs.ibr.cc;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = false)
public class FacebookService {
    @JsonProperty(required = true)
    private String ID;

    @JsonProperty("Node")
    private String Node;

    @JsonProperty("Address")
    private String Address;

    @JsonProperty("Datacenter")
    private String Datacenter;

    @JsonProperty("TaggedAddresses")
    private String TaggedAddresses;

    @JsonProperty("NodeMeta")
    private String NodeMeta;

    @JsonProperty("ServiceKind")
    private String ServiceKind;

    @JsonProperty("ServiceID")
    private String ServiceID;

    @JsonProperty("ServiceName")
    private String ServiceName;

    @JsonProperty("ServiceTags")
    private String[] ServiceTags;

    @JsonProperty("ServiceAddress")
    private String ServiceAddress;

    @JsonProperty("ServiceWeights")
    private Map<String, String> ServiceWeights;

    @JsonProperty("ServiceMeta")
    private Map<String, String> ServiceMeta;

    @JsonProperty("ServicePort")
    private Integer ServicePort;

    @JsonProperty("ServiceEnableTagOverride")
    private Boolean ServiceEnableTagOverride;

    @JsonProperty("ServiceProxyDestination")
    private String ServiceProxyDestination;

    @JsonProperty("ServiceProxy")
    private Map<String, String> ServiceProxy;


    @JsonProperty("ServiceConnect")
    private Map<String, String> ServiceConnect;


    @JsonProperty("CreateIndex")
    private Integer CreateIndex;


    @JsonProperty("ModifyIndex")
    private Integer ModifyIndex;


    // ... getter and setter
    public String getID() {
        return this.ID;
    }

    public String getNode() {
        return this.Node;
    }

    public String getAddress() {
        return this.Address;
    }

    public String getDatacenter() {
        return this.Datacenter;
    }

    public String getTaggedAddresses() {
        return this.TaggedAddresses;
    }

    public String getNodeMeta() {
        return this.NodeMeta;
    }

    public String getServiceKind() {
        return this.ServiceKind;
    }

    public String getServiceID() {
        return this.ServiceID;
    }

    public String getServiceName() {
        return this.ServiceName;
    }

    public String[] getServiceTags() {
        return this.ServiceTags;
    }

    public String getServiceAddress() {
        return this.ServiceAddress;
    }

    public Map<String, String> getServiceWeights() {
        return this.ServiceWeights;
    }

    public Map<String, String> getServiceMeta() {
        return this.ServiceMeta;
    }

    public Integer getServicePort() {
        return this.ServicePort;
    }

    public Boolean getServiceEnableTagOverride() {
        return this.ServiceEnableTagOverride;
    }

    public String getServiceProxyDestination() {
        return this.ServiceProxyDestination;
    }

    public Map<String, String> getServiceProxy() {
        return this.ServiceProxy;
    }

    public Map<String, String> getServiceConnect() {
        return this.ServiceConnect;
    }

    public Integer getCreateIndex() {
        return this.CreateIndex;
    }

    public Integer getModifyIndex() {
        return this.ModifyIndex;
    }

    public void setID(String id) {
        this.ID = id;
    }

    public String getIP() {
        return "http://" + getAddress() + ":" + getServicePort();
    }
}