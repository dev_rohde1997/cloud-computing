package main.java.de.tubs.ibr.cc;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;


@JsonIgnoreProperties(ignoreUnknown = false)

public class ServiceApiDocumentation {

    @JsonProperty("swagger")
    private String swagger;

    @JsonProperty("info")
    private JsonNode info;


    @JsonProperty("host")
    private String host;


    @JsonProperty("basePath")
    private String basePath;

    @JsonProperty("tags")
    private JsonNode[] tags;

    @JsonProperty("paths")
    private JsonNode paths;

    @JsonProperty("definitions")
    private JsonNode definitions;

    public String getSwagger() {
        return this.swagger;
    }

    public JsonNode getInfo() {
        return this.info;
    }

    public String getHost() {
        return this.host;
    }

    public String getBasePath() {
        return this.basePath;
    }

    public JsonNode[] getTags() {
        return this.tags;
    }

    public JsonNode getPaths() {
        return this.paths;
    }

    public JsonNode getDefinitions() {
        return this.definitions;
    }
}
