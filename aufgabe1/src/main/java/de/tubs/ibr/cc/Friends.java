package main.java.de.tubs.ibr.cc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = false)
public class Friends {


    @JsonProperty("ids")
    private List<String> ids;

    public List<String> getIds() {
        return ids;
    }
}
