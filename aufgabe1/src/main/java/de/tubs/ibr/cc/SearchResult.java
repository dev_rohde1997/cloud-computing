package main.java.de.tubs.ibr.cc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SearchResult {
    @JsonIgnoreProperties(ignoreUnknown = false)

    @JsonProperty("result")
    private List<Person> result;

    @JsonProperty("offset")
    private String offset;

    @JsonProperty("length")
    private String length;

    public List<Person> getResult() {
        return result;
    }

    public String getOffset() {
        return offset;
    }

    public String getLength() {
        return length;
    }

}
