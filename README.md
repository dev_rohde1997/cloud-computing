# Cloud Computing

This project was part of the lecture **Cloud Computing** at TU Braunschweig.
Part of the project was also the dynamic deployment at AWS EC2 instances.

Processing period: Apr. 2020 - Aug. 2020


## Features:

- **AWS EC2** compatibility
- **Consul** service mesh
- **Ansible** for cloud installation
- **Terrform** for dynamic reservation of resources
- **Fabio** for health checks
- **Grafana** for monitoring
- **Spring Boot** for service application
