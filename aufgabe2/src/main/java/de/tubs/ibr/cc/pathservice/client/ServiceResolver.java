package de.tubs.ibr.cc.pathservice.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.tubs.ibr.cc.pathservice.jackson.Service;
import de.tubs.ibr.cc.pathservice.jackson.Friends;
import de.tubs.ibr.cc.pathservice.jackson.Person;
import de.tubs.ibr.cc.pathservice.jackson.ServiceApiDocumentation;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ServiceResolver {

    //CONSTANTS

    //URI - Values
    public String ip_address = "http://127.0.0.1:8500";
    public final static String SUB_URI_SERVICE = "/v1/catalog/service/";
    public final static String SUB_URI_SERVICES = "/v1/catalog/services";
    public final static String SUB_URI_API = "/v2/api-docs";

    //TOKEN - Values
    public final static String TOKEN_VALUE = "1234";
    //public final static String TOKEN_VALUE = "2905b0d2-89c7-6b07-4a7a-e4684cc5e2d9";
    public final static String HEADER_X_CONSUL_TOKEN_NAME = "X-Consul-Token";
    public final static String HEADER_CONTENT_TYPE = "Content-Type";
    public final static String HEADER_CONTENT_TYPE_JSON = "application/json";

    //Name of our Service
    public static final String FACEBOOK_SERVICE = "Service";

    //Local Consul Data
    public static final String PATH_SERVICE = "PathService";
    public static final String PATH_SERVICE_ID = "PathService";
    public static final String CONSUL_SERVICE_ADDRESS = "127.0.0.1";
    public static final String CONSUL_SERVICE_ADDRESS_NETWORK = "192.168.178.21";
    public static final int CONSUL_SERVICE_PORT = 8500;

    //Vars
    HttpRequest httpRequest = new HttpRequest();

    public ServiceResolver(String ip) {
        this.ip_address = "http://" + ip;
    }

    public ServiceResolver() {}


    // --- METHODS ---

    //List of all services addresses
     String getServiceAddress(String serviceName) throws JsonProcessingException, IOException{
        //Service service = getService(serviceName);
        //return "http://" + service.getAddress() + ":" + service.getServicePort();
        //return "http://127.0.0.1:8500"; //+ + service.getServicePort();
        return ip_address;
    }

    //List of all services endpoints
    public ServiceApiDocumentation listServiceEndpoints(String serviceName) throws JsonProcessingException, IOException{
        String api_response = getApiDocs(getServiceAddress(serviceName));

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(api_response, ServiceApiDocumentation.class);
    }

    //List of all available services
    public Set<String> listServices() throws IOException {

        String serviceAddress = httpRequest.startHttpRequest_GET(ip_address + SUB_URI_SERVICES);
        if (serviceAddress == null) return null;

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> jsonMap = mapper.readValue(serviceAddress,
                new TypeReference<>(){});

        return jsonMap.keySet();
    }

    //Search for person by name
    SearchResult searchPerson(String name, int length, int offset, String regex)
            throws IllegalArgumentException, IOException {

        String uri = buildURI(name, length, offset, regex);
        String serviceString = httpRequest.startHttpRequest_GET(uri);
        assert serviceString != null;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, SearchResult.class);
    }

    //Search for friends by user id
    Person searchFriends(String user_id)
            throws IllegalArgumentException, IOException {

        String uri = getServiceAddress(FACEBOOK_SERVICE);
        uri += "/facebook/user/" + user_id;

        String serviceString = httpRequest.startHttpRequest_GET(uri);
        assert serviceString != null;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, Person.class);
    }

    //Search for friends batches by user ids
    Friends searchFriendsBatch(List<String> user_ids)
            throws IllegalArgumentException, IOException {

        String uri = getServiceAddress(FACEBOOK_SERVICE);
        uri += "/facebook/friends";
        //System.out.println(uri);
        StringBuilder json = new StringBuilder("");
        addArrayToJSON(user_ids, json);

        String serviceString = httpRequest.startHttpRequest_POST(uri, json.toString());
        assert serviceString != null;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, Friends.class);
    }

    // --- HELPER METHODS ---

    //Get API endpoints from docs
    private String getApiDocs(final String URL) {
            return httpRequest.startHttpRequest_GET(URL + SUB_URI_API);
    }

    //Requesting facebook service data
    private Service getService(String service_name) throws JsonProcessingException, IOException {
        if (service_name == null) return null;
        String uri = ip_address + SUB_URI_SERVICE + service_name;
        //System.out.println(uri);

        String serviceString = httpRequest.startHttpRequest_GET(uri);
        if (serviceString == null) return null;

        //System.out.println(serviceString);
        serviceString = serviceString.substring(1,serviceString.length() - 1);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(serviceString, Service.class);
    }

    //Building a URI by given params
    private String buildURI(String name, int length, int offset, String regex) throws JsonProcessingException, IOException {
        StringBuilder uri = new StringBuilder(getServiceAddress(FACEBOOK_SERVICE));
        uri.append("/facebook/search");

        if (name == null) throw new IllegalArgumentException();
        uri.append("?");
        if (length > 0) {
            uri.append("length=");
            uri.append(length);
            uri.append("&");
        }

        if (offset > 0) {
            uri.append("offset=0");
            uri.append("&");
        }

        uri.append("query=");
        uri.append(name);
        uri.append("&");


        if (regex != null) {
            uri.append("regex=");
            uri.append(regex);
            uri.append("&");
        }

        uri.delete(uri.length()-1, uri.length());
        return uri.toString();
    }

    //Building a json string by a given List of Strings
    //
    private void addArrayToJSON(List<String> user_ids, StringBuilder json) {
        json.append("[");
        for (String s : user_ids) {
            json.append("\"");
            json.append(s);
            json.append("\"");
            json.append(",");
        }
        json.delete(json.length()-1, json.length()); //Remove last ,
        json.append("]");
    }

    //Register service at local consul service
    public void registerService(String serviceName, String serviceId, String serviceAddress, int servicePort) {

        String consul_address = "http://"
                + CONSUL_SERVICE_ADDRESS
                + ":" + CONSUL_SERVICE_PORT
                + "/v1/agent/service/register";

        try {
            String json = buildRegisterServiceJSON(serviceName, serviceId, serviceAddress, servicePort);
            HttpResponse<String> response = httpRequest.startHttpRequest_PUT(consul_address, json);

            if (response != null && response.statusCode() == 200) {
                System.out.println("Service successfully registered!");
            } else {
                System.out.println("Failed to register service! Response Code: " + response.statusCode());
            }

        } catch (JsonProcessingException e) {
            System.out.println("Error with creating JSON for registering Service: " + e);
        }
    }

    //Deregister service at local consul service
    public void deregisterService(String serviceId) {
        String consul_address = "http://"
                + CONSUL_SERVICE_ADDRESS
                + ":" + CONSUL_SERVICE_PORT
                + "/v1/agent/service/deregister"
                + "/" + serviceId;

        HttpResponse<String> response = httpRequest.startHttpRequest_PUT(consul_address, "");

        if (response != null && response.statusCode() == 200) {
            System.out.println("Service successfully deregistered!");
        } else {
            System.out.println("Failed to deregister service!");
        }
    }

    //Creating a JSON String for register function
    private String buildRegisterServiceJSON(String name, String id, String address, int port) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode service = mapper.createObjectNode();
        service.put("Name", name);
        service.put("Id", id);
        service.put("Address", address);
        service.put("Port", port);
        //service.put("Tag Value", "urlprefix-/");

        return mapper.writeValueAsString(service);
    }

    /*
    * {
    *   "Name" : "name",
    *   "Port" : "port",
    *   "Address": "address",
    *   "Id" : id
    * }
    * */
}
