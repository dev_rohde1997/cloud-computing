package de.tubs.ibr.cc.pathservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import de.tubs.ibr.cc.pathservice.configuration.properties.ApplicationProperties;


@RestController
public class HelloRestController {
  private final ApplicationProperties applicationProperties;


  // ApplicationProperties are injected by Spring
  public HelloRestController(ApplicationProperties applicationProperties) {
    this.applicationProperties = applicationProperties;
  }


  @GetMapping("/hello")
 // Example call: "/hello?name=World"
  public String hello(
      @PathVariable final String name) {
    return "Hello, " + name + "!";
  }
}
