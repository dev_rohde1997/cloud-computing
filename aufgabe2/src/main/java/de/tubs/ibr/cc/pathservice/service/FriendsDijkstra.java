package de.tubs.ibr.cc.pathservice.service;

import de.tubs.ibr.cc.pathservice.model.IPerson;

import java.util.*;
import java.util.stream.Collectors;


public class FriendsDijkstra {

	/**
	 * Calculates the shortest path between startID and endID.
	 */
	public static Optional<List<IPerson>> getShortestPath(Set<? extends IPerson> graph, String startID, String endID) {

		System.out.println("DIJKSTRA: " + graph.size() + " nodes, startID = " + startID + ", endID = " + endID);
		long startTime = System.currentTimeMillis();

		Map<String, Node> graphMap = new HashMap<>();
		for (IPerson person : graph) {
			Node node = new Node(person);
			graphMap.put(person.getId(), node);
		}

		Set<Node> settledNodes = new HashSet<>();
		Set<Node> unsettledNodes = new HashSet<>();

		Node startNode = graphMap.get(startID);
		startNode.distance = 0;
		unsettledNodes.add(startNode);

		while(unsettledNodes.size() != 0) {
			Node currentNode = getLowestDistanceNode(unsettledNodes);
			unsettledNodes.remove(currentNode);
			for (Node adjacentNode: getAdjacentNodes(graphMap, currentNode)) {
				if (!settledNodes.contains(adjacentNode)) {
					calculateMinimumDistance(adjacentNode, currentNode);
					unsettledNodes.add(adjacentNode);
				}
			}
			settledNodes.add(currentNode);
		}
		System.out.println("DIJKSTRA: completed in " + (System.currentTimeMillis() - startTime) + "ms");

		Node endNode = graphMap.get(endID);
		if(endNode.distance == Integer.MAX_VALUE) {
			return Optional.empty();
		}
		List<IPerson> shortestPath = endNode.shortestPath.stream().map(node -> node.person).collect(Collectors.toList());
		return Optional.of(shortestPath);
	}

	private static void calculateMinimumDistance(Node evaluationNode, Node sourceNode) {
		Integer sourceDistance = sourceNode.distance;
		if (sourceDistance + 1 < evaluationNode.distance) {
			evaluationNode.distance = sourceDistance + 1;
			List<Node> shortestPath = new ArrayList<>(sourceNode.shortestPath);
			shortestPath.add(sourceNode);
			evaluationNode.shortestPath = shortestPath;
		}
	}

	private static Node getLowestDistanceNode(Set <Node> unsettledNodes) {
		Node lowestDistanceNode = null;
		Optional<Node> nearestNode = unsettledNodes.stream().min(Comparator.comparing(node -> node.distance));
		return nearestNode.orElseThrow(NoNodesLeftException::new);
	}

	private static List<Node> getAdjacentNodes(Map<String, Node> graph, Node node) {
		return node.person.getFriends().stream().map(graph::get).filter(Objects::nonNull).collect(Collectors.toList());
	}

	private static class Node {
		final IPerson person;
		List<Node> shortestPath = new ArrayList<>();
		Integer distance = Integer.MAX_VALUE;

		private Node(IPerson person) {
			this.person = person;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof Node)) return false;
			Node node = (Node) o;
			return person.getId().equals(node.person.getId());
		}

		@Override
		public int hashCode() {
			return Objects.hash(person.getId());
		}
	}

	private static class NoNodesLeftException extends RuntimeException {
	}
}
