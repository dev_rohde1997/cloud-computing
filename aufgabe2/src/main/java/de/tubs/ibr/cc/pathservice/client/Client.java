package de.tubs.ibr.cc.pathservice.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.tubs.ibr.cc.pathservice.jackson.Friends;
import de.tubs.ibr.cc.pathservice.jackson.Person;
import de.tubs.ibr.cc.pathservice.jackson.ServiceApiDocumentation;
import de.tubs.ibr.cc.pathservice.model.IPerson;
import de.tubs.ibr.cc.pathservice.service.PathService;

import java.io.IOException;
import java.util.*;

import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.CONSUL_SERVICE_ADDRESS;
import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.CONSUL_SERVICE_PORT;

/**
 * CLI application entry point
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("Facebook Command Line Client");


        //Init args
        if (args.length < 1 | args.length > 4) throw new IllegalArgumentException();

        String option_1 = args[0];
        String option_2 = null;
        String option_3 = null;
        String option_4 = null;

        if (args.length > 1)
            option_2 = args[1];

        if (args.length > 2)
            option_3 = args[2];

        if (args.length > 2)
            option_4 = args[3];


        try {
            ServiceResolver serviceResolver = new ServiceResolver();
            switch (option_1) {
                case "endpoints":
                    System.out.println("List of all endpoints:");
                    List<String> endpoints = getEndpoints(option_2);
                    printListFormat(endpoints);
                    break;
                case "services":
                    Set<String> service_list = serviceResolver.listServices();
                    printListFormat(service_list);
                    break;
                case "search":
                    System.out.println("Searching for " + option_2);
                    System.out.println(searchPersons(option_2));
                    break;
                case "friends":
                    Person p1 = serviceResolver.searchFriends(option_2);
                    System.out.printf("Searching for friends of %s (%s)%n", p1.getName(), p1.getId());
                    for (String friends_id : p1.getFriends()) {
                        Person friend = serviceResolver.searchFriends(friends_id);
                        System.out.printf("\t- %s (%s)%n", friend.getName(), friend.getId());
                    }
                    break;
                case "friends_batch":
                    System.out.println("Searching for friends batch");
                    Person p  = searchPersons("Moritz").get(0);
                    printListFormat(getFriendsBatch(p.getFriendsList()));
                    break;
                case "path":
                    System.out.printf("Calculate path for %s and %s%n", option_2, option_3);
                    getPath(option_2, option_3, option_4);
                    break;
                default:
                    System.err.println("Unknown option_1");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Fehler");
        }
    }

    public static List<Person> searchPersons(String name) throws JsonProcessingException, IOException {
        ServiceResolver serviceResolver = new ServiceResolver();
        SearchResult result = serviceResolver.searchPerson(name, 10, 0, null);
        return result.getResult();
    }

    public static List<String> getFriends(String id) throws IOException {
        ServiceResolver serviceResolver = new ServiceResolver();
        Person p = serviceResolver.searchFriends(id);
        return p.getFriends();
    }

    public static Person getPerson(String id) throws IOException {
        ServiceResolver serviceResolver = new ServiceResolver();
        Person p = serviceResolver.searchFriends(id);
        return p;
    }

    public static List<String> getFriendsBatch(List<String> ids) throws IOException {
        ServiceResolver serviceResolver = new ServiceResolver();
        Friends friends = serviceResolver.searchFriendsBatch(ids);
        return friends.getIds();
    }

    public static List<String> getEndpoints(String service_name) throws IOException{
        ServiceResolver serviceResolver = new ServiceResolver();
        ServiceApiDocumentation docs = serviceResolver.listServiceEndpoints(service_name);
        Iterator<String> iterator = docs.getPaths().fieldNames();
        ArrayList<String> endpointList = new ArrayList<>();

        while (iterator.hasNext()) {
            endpointList.add(iterator.next());
        }

        return endpointList;
    }

     private static void printListFormat(Iterable<String> list) {
        for (String key : list) {
            System.out.println("\t-" + key);
        }
    }

    /** Calculating path request to service
     *
     * @param id1 First user
     * @param id2 Second user
     * @param ip Consul IP address
     * @throws IOException can occure by parsing json response
     */
    public static void getPath(String id1, String id2, String ip) throws IOException{
        ServiceResolver s = new ServiceResolver(ip);
        String address = s.getServiceAddress("PathService");
        HttpRequest httpRequest = new HttpRequest();
        System.out.println(address);
        String url = address + "/path/" + id1 + "/" + id2;
        System.out.println(url);
        String response = httpRequest.startHttpRequest_GET(url);
        System.out.println(response);
    }

    private static void printIPersonOptional(Optional<List<IPerson>> optional, String id2) {
        System.out.println("Distance: " + optional.get().size());

        for (IPerson p : optional.get()) {
            System.out.printf("%s (%s) -> ", p.getName(), p.getId());
        }

        try {
            Person p = Client.getPerson(id2);
            System.out.printf("%s (%s)%n", p.getName(), p.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
