package de.tubs.ibr.cc.pathservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tubs.ibr.cc.pathservice.NoPathException;
import de.tubs.ibr.cc.pathservice.client.Client;
import de.tubs.ibr.cc.pathservice.client.SearchResult;
import de.tubs.ibr.cc.pathservice.jackson.Person;
import de.tubs.ibr.cc.pathservice.jackson.Response;
import de.tubs.ibr.cc.pathservice.model.IPerson;
import de.tubs.ibr.cc.pathservice.service.PathService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController public class PathController {

    private PathService pathService;

    public PathController(PathService pathService) {
        this.pathService = pathService;
    }

    @GetMapping ("/path/{id1}/{id2}")
    public String calculatePath(@PathVariable String id1, @PathVariable String id2)  {
        System.out.println("Calculate: " + id1  + " -> " + id2);
        Optional<List<IPerson>> result = null;
        long distance = 0L;
        ObjectMapper mapper = new ObjectMapper();
        Response response = new Response();
        try {
            result = pathService.shortestPath(id1, id2);
            if (result.isPresent())
                distance = result.get().size();
        } catch (NoPathException e) {
            response.setStatus_code(404);
            response.setMessage("No Path found");
        }



        try {
            if (result != null & response.getStatus_code() != 404) {
                List<IPerson> persons = result.get();
                Person p = null;
                try {
                    p = Client.getPerson(id2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (p != null)
                    persons.add(p);

                response.setStatus_code(200);
                response.setMessage("Path found with distance " + distance
                        + mapper.writeValueAsString(persons.stream().map(IPerson::getName).collect(Collectors.toList())));
            }
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {

        }
        response.setStatus_code(500);
        response.setMessage("Internal Server Error");
        return response.toString();
    }


}
