package de.tubs.ibr.cc.pathservice.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.tubs.ibr.cc.pathservice.model.IPerson;

import java.util.List;
import java.util.Optional;

public class Response {

    @JsonProperty("status_code")
    private int status_code;

    @JsonProperty("message")
    private String message;


    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return getStatus_code() + " - " + getMessage();
    }
}
