package de.tubs.ibr.cc.pathservice.model;

import java.util.List;

/**
 * Glue class for Person and {@link de.tubs.ibr.cc.pathservice.service.FriendsDijkstra}
 *  - Person should implement this.
 */
public interface IPerson {
  String getName();
  String getId();
  List<String> getFriends();
}
