package de.tubs.ibr.cc.pathservice.jackson;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.tubs.ibr.cc.pathservice.model.IPerson;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = false)
public class Person implements IPerson {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("friends")
    private List<String> friends;

    public void setID(String ID) {
        this.id = ID;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    @Override
    public String toString() {
        return getId() + ", " + getName() + ", " + getFriends();
    }

    public List<String> getFriendsList() {
        return getFriends();

    }
}