package de.tubs.ibr.cc.pathservice;

import de.tubs.ibr.cc.pathservice.client.ServiceResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.CONSUL_SERVICE_ADDRESS;
import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.PATH_SERVICE_ID;
import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.PATH_SERVICE;
import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.CONSUL_SERVICE_PORT;


@SpringBootApplication
public class Application {


  public static void main(String[] args) {

      //ServiceResolver serviceResolver;
     // if (args.length > 0)
         // serviceResolver = new ServiceResolver(args[1]);

      ServiceResolver serviceResolver = new ServiceResolver();

      serviceResolver.registerService(
              PATH_SERVICE,
              PATH_SERVICE_ID,
              CONSUL_SERVICE_ADDRESS,
              CONSUL_SERVICE_PORT
      );

      Runtime.getRuntime().addShutdownHook(new Thread(() -> {
          try {
              Thread.sleep(200);
              serviceResolver.deregisterService(PATH_SERVICE_ID);
              System.out.println("Shutting down ...");

          } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
              e.printStackTrace();
          }
      }));


      SpringApplication.run(Application.class);
  }

}
