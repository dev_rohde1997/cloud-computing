package de.tubs.ibr.cc.pathservice.jackson;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = false)
public class Friends {


    @JsonProperty("ids")
    private List<String> ids;

    public List<String> getIds() {
        return ids;
    }
}
