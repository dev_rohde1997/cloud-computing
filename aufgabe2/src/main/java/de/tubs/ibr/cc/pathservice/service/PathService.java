package de.tubs.ibr.cc.pathservice.service;

import de.tubs.ibr.cc.pathservice.NoPathException;
import de.tubs.ibr.cc.pathservice.client.Client;
import de.tubs.ibr.cc.pathservice.jackson.Person;
import de.tubs.ibr.cc.pathservice.model.IPerson;
import org.springframework.stereotype.Service;
import java.util.*;

@Service public class PathService {

    //Example:
    //http://127.0.0.1:8080/path/100000054272073/1207742989
    public Optional<List<IPerson>> shortestPath(String id1, String id2) throws NoPathException {
        Optional<List<IPerson>> optional = null;
        try {
            List<String> ids = new ArrayList<>();
            Set<Person> person_set = new HashSet<>();
            person_set.add(Client.getPerson(id1));
            person_set.add(Client.getPerson(id2));

            while (person_set.size() < 10000) {
                for (Person p : person_set) {
                    ids.add(p.getId());
                }

                List<String> friends_batch = Client.getFriendsBatch(ids);

                int old_size = person_set.size();
                for (String item : friends_batch) {
                    Person p = Client.getPerson(item);
                    person_set.add(p);
                }
                int new_size = person_set.size();
                //No graph connection
                if (old_size == new_size)
                    throw new NoPathException();


                optional = FriendsDijkstra.getShortestPath(person_set, id1, id2);

                if (!optional.isEmpty()) {
                    return optional;
                }
            }
            System.out.println(ids);

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Parsing error");
        }
        throw new NoPathException();
    }

}
