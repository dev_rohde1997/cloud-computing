package de.tubs.ibr.cc.pathservice.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.tubs.ibr.cc.pathservice.jackson.Person;

import java.util.List;

public class SearchResult {
    @JsonIgnoreProperties(ignoreUnknown = false)

    @JsonProperty("result")
    private List<Person> result;

    @JsonProperty("offset")
    private String offset;

    @JsonProperty("length")
    private String length;

    public List<Person> getResult() {
        return result;
    }

    public String getOffset() {
        return offset;
    }

    public String getLength() {
        return length;
    }

}
