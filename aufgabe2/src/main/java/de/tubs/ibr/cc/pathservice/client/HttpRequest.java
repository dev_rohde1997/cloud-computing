package de.tubs.ibr.cc.pathservice.client;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;

import static de.tubs.ibr.cc.pathservice.client.ServiceResolver.*;

public class HttpRequest {

    private HttpClient client = HttpClient.newHttpClient();

    //Simple GET Request
    String startHttpRequest_GET(String uri) {
        //System.out.println("My URI: " + uri);
        try {

            java.net.http.HttpRequest request = java.net.http.HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .header(HEADER_X_CONSUL_TOKEN_NAME, TOKEN_VALUE)
                    .build();
            return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (InterruptedException| IOException | NullPointerException e1 ) {
            e1.printStackTrace();
        }
        return null;
    }

    //Simple POST Request
    String startHttpRequest_POST(String uri, String json) {
        try {
            java.net.http.HttpRequest request = java.net.http.HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .header(HEADER_X_CONSUL_TOKEN_NAME, TOKEN_VALUE)
                    .header(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_JSON)
                    .POST(java.net.http.HttpRequest.BodyPublishers.ofString(json))
                    .build();
            return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (InterruptedException|IOException|NullPointerException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    //Simple PUT Request
    HttpResponse<String> startHttpRequest_PUT(String uri, String json) {
        try {
            java.net.http.HttpRequest request = java.net.http.HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .header(HEADER_X_CONSUL_TOKEN_NAME, TOKEN_VALUE)
                    .header(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_JSON)
                    .PUT(java.net.http.HttpRequest.BodyPublishers.ofString(json))
                    .build();
            return client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException|IOException|NullPointerException e1) {
            e1.printStackTrace();
        }
        return null;
    }

}
