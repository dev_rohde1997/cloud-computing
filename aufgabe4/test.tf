#./terraform plan -var "role=consul_server" -var "private_ssh_key=ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTtmX7ZH38bhidg/BGgP6R2v7w1kwMF0qD9Y9La8qXDCPONYut0mdDRAipfrHQzQLkcLDiMzo5xvqLvEGtbAa6motY8hBpc/i4WXxaOnrqkKhskE3C622CCWs3OpgmUB2xsyD40hHX/HtFwydW/3SbvXV4rpi/QF++UTmSw6jLDeXZxTwLhPizToQ7xG1yro6zo1wRAHuLCubAieAonhg/ft7A6Xw8T3hxnV2WA7vAK/QNzxwzrruIXx6q3prrzKHsPhqzA1rldjUK9kLe2mlTNj3Gl7saCGy7LTsTqoVnMgbc8vZ9QLvK/kl+0uKCVNnhLjSWOsh6NhcOuoVroAD/ carl@Carls-MacBook-Pro.local"
#./terraform plan -var "role=consul_server"

#init plan apply


provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "tubs-ibr-cc-team009"
    region = "us-east-1"
    key = "terraform.tfstate"
  }
}

variable "role" {
  type=string
}

resource "aws_key_pair" "ssh_key" {
  key_name = "team009"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}


resource "aws_instance" "team009" {
  key_name = "${aws_key_pair.ssh_key.key_name}"
  ami = "ami-0e71ec19ea9a4cb0b"
  instance_type           = "t3a.micro"
  subnet_id               = "subnet-d0e5dbfb"
  vpc_security_group_ids  = ["sg-04f3e87693b39a126"]
  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Role  = var.role
    Group = "team009"
  }
}


output "test" {
  value = "${aws_key_pair.ssh_key.key_name}"
}
